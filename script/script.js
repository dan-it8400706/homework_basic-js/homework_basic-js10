
let ul = document.querySelector('.tabs');
ul.addEventListener('click', function (ev) {
    let data = ev.target.dataset.toggle;
    document.querySelector('.visible').classList.remove('visible');
    document.querySelector('.active-tab').classList.remove('active-tab');
    document.querySelector(`#text${data}`).classList.add('visible')
    ev.target.classList.add('active-tab')
});